package mx.tec.clase3

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.fragment.app.Fragment

class MainActivity : AppCompatActivity(), DoggoFragment.Callback {

    lateinit var demoFragment: DemoFragment
    lateinit var doggoFragment: DoggoFragment


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // podemos crear un nuevo objeto de fragmento utilizando el constructor
        demoFragment = DemoFragment()
        doggoFragment = DoggoFragment.Companion.newInstance("fido", 5)

        // 1era forma - hacia el fragmento
        // modificar fragmentos desde código
        // hay un objeto que existe para modificar la presencia de fragmentos en actividad
        // supportFragmentManager

        // hay necesidad de hacer transacciones con el manager
        // para modificar contenidos de fragmentos
        val transaction = supportFragmentManager.beginTransaction()

        transaction.add(R.id.fragmentContainerView, doggoFragment, TAG_FRAGMENTO)
        transaction.commit()

    }

    fun intercambiarFragmentos(view: View?){

        // cambiar a fragmento distinto únicamente al presionar botón
        val fragmentoActual = supportFragmentManager.findFragmentByTag(TAG_FRAGMENTO)

        // null si no encontró fragmento con el tag que pusimos
        if(fragmentoActual != null){

            var fragmentoNuevo : Fragment = doggoFragment

            /*
            if(fragmentoActual == demoFragment)
                fragmentoNuevo = doggoFragment
*/

            if(fragmentoActual == doggoFragment)
                fragmentoNuevo = demoFragment

            val transaction = supportFragmentManager.beginTransaction()
            /*
            transaction.remove(fragmentoActual)
            transaction.add(R.id.fragmentContainerView, fragmentoNuevo, TAG_FRAGMENTO)
            */

            transaction.replace(R.id.fragmentContainerView, fragmentoNuevo, TAG_FRAGMENTO)
            transaction.commit()


        }
    }

    fun interactuarConFragmento(view: View?){

        doggoFragment.hacerLog()
    }

    fun irAFirebase(view: View?) {

        val intent = Intent(this, FirebaseActivity::class.java)
        startActivity(intent)
    }

    fun irARecycler(view: View?) {

        val intent = Intent(this, RecyclerActivity::class.java)
        startActivity(intent)
    }

    override fun ejecutar() {
        Toast.makeText(this, "EJECUTADO DESDE FRAGMENTO", Toast.LENGTH_SHORT).show()
    }

    companion object {

        private const val TAG_FRAGMENTO = "fragmentito"
    }
}