package mx.tec.clase3

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class PerritoAdapter(private var perritos : ArrayList<String>,
                     private var listener : View.OnClickListener)
    : RecyclerView.Adapter<PerritoAdapter.PerritoViewHolder>()  {

    // lo primero es definir ViewHolder
    // ViewHolder es un objeto que sirve para manipular a una vista específica
    // que normalmente va a ser la que usemos como nuestro elemento
    class PerritoViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView) {

        var texto1 : TextView
        var texto2 : TextView

        // init block
        // código de inicialización que SIEMPRE corre
        init {

            texto1 = itemView.findViewById(R.id.row_text1)
            texto2 = itemView.findViewById(R.id.row_text2)
        }

    }

    // cuando crea la vista
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PerritoViewHolder {

        // igual que con fragmentos hay que inflar interfaz
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.row, parent, false)

        val button = view.findViewById<Button>(R.id.row_button)

        button.setOnClickListener {
            Log.wtf("ADAPTER", "HOLA!")
        }

        view.setOnClickListener(listener)

        return PerritoViewHolder(view)
    }

    // cuando asocia una vista con un elemento en particular de la colección
    override fun onBindViewHolder(holder: PerritoViewHolder, position: Int) {
        holder.texto1.text = perritos[position]
        holder.texto2.text = perritos[position]
    }

    // obtener total de elementos
    override fun getItemCount(): Int {
        return perritos.size
    }
}