package mx.tec.clase3

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Parcel
import android.os.Parcelable
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class RecyclerActivity() : AppCompatActivity(), View.OnClickListener {

    lateinit var recyclerView : RecyclerView
    lateinit var datos : ArrayList<String>

    constructor(parcel: Parcel) : this() {

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recycler)

        // recycler view - vista (widget) para desplegar objetos que conformen
        // un conjunto / colección / estructura / etc

        // GUI (muy concreta) - Adapter (traductor de datos para GUI) - DATOS (abstractos)
        recyclerView = findViewById(R.id.recyclerView)

        datos = ArrayList()
        datos.add("Fido")
        datos.add("Fifi")
        datos.add("Firulais")
        datos.add("Kaiser")
        datos.add("Killer")
        datos.add("Fido")
        datos.add("Fifi")
        datos.add("Firulais")
        datos.add("Kaiser")
        datos.add("Killer")
        datos.add("Fido")
        datos.add("Fifi")
        datos.add("Firulais")
        datos.add("Kaiser")
        datos.add("Killer")
        datos.add("Fido")
        datos.add("Fifi")
        datos.add("Firulais")
        datos.add("Kaiser")
        datos.add("Killer")

        // crear adaptador
        val adapter = PerritoAdapter(datos, this)

        // es necesario declarar un layout manager
        // este define cómo se organizan los elementos en el recyclerView
        val llm = LinearLayoutManager(this)
        llm.orientation = LinearLayoutManager.VERTICAL
        val glm = GridLayoutManager(this, 2)

        // asignamos layout manager
        recyclerView.layoutManager = llm
        recyclerView.adapter = adapter


    }

    override fun onClick(row : View) {
        val position = recyclerView.getChildLayoutPosition(row)
        Toast.makeText(this, datos[position], Toast.LENGTH_SHORT).show();
    }


}