package mx.tec.clase3

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.EditText
import android.widget.Toast
import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.DocumentChange
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase

class FirebaseActivity : AppCompatActivity() {

    lateinit var login : EditText
    lateinit var password : EditText
    lateinit var nombre : EditText
    lateinit var edad : EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_firebase)

        login = findViewById(R.id.firebase_login)
        password = findViewById(R.id.firebase_password)
        nombre = findViewById(R.id.firebase_nombre)
        edad = findViewById(R.id.firebase_edad)
    }


    fun registro(view: View?){

        Firebase.auth.createUserWithEmailAndPassword(login.text.toString(), password.text.toString())
            .addOnCompleteListener(this) {
                if(it.isSuccessful){

                    Log.d("FIREBASE", "Registro exitoso")
                } else {

                    Log.w("FIREBASE", "Registro fracaso ${it.exception}")
                }
            }
    }

    fun login(view: View?){

        Firebase.auth.signInWithEmailAndPassword(login.text.toString(), password.text.toString())
            .addOnCompleteListener(this){

                if(it.isSuccessful){

                    Log.d("FIREBASE LOGIN", "Login exitoso")
                } else {
                    Log.e("FIREBASE LOGIN", "error: ${it.exception?.message}")
                }
            }
    }

    fun logout(view: View?){

        Firebase.auth.signOut()
    }

    override fun onStart() {
        super.onStart()
        verificarUsuarioGeneral()
        // cada vez que tu app empiece
        // (o reempiece) verificar si tienes un usuario válido
    }

    fun verificarUsuarioGeneral(){

        if(Firebase.auth.currentUser == null)
            Toast.makeText(this, "SIN USUARIO!", Toast.LENGTH_SHORT).show()
        else
            Toast.makeText(this, Firebase.auth.currentUser?.email, Toast.LENGTH_SHORT).show()
        // si el usuario no está registrado hacer algo con la GUI
        // POR EJEMPLO: mandar a login
    }

    fun verificarUsuario(view : View?){
        verificarUsuarioGeneral()
    }

    fun registrarDatos(view : View?) {

        val perrito = hashMapOf(
            "nombre" to nombre.text.toString(),
            "edad" to edad.text.toString().toInt()
        )

        Firebase.firestore.collection("perritos")
            .add(perrito)
            .addOnSuccessListener { documentReference ->
                Log.d("FIRESTORE", "id: ${documentReference.id}")
            }
            .addOnFailureListener{ e ->
                Log.e("FIRESTORE", "hubo error: $e")
            }
    }

    fun solicitarDatos(view : View?){


        // solicitud "NORMAL"
        // no se actualiza en tiempo real, solo solicita y recibe resultados
        Firebase.firestore.collection("perritos")
            .get()
            .addOnSuccessListener { result ->

                // recorrer datos
                for(doc in result){
                    Log.d("FIRESTORE", "${doc.id} ${doc.data}")
                }
            }
            .addOnFailureListener{ e ->
                Log.e("FIRESTORE", "Errorcito: $e")
            }

        // 2da opcion - suscribirse a actualizaciones en tiempo real
        Firebase.firestore.collection("perritos").addSnapshotListener{ datos, e ->

            // verificar error y terminar ejecucion
            if(e != null){

                Log.e("FIRESTORE", "error: $e")
                return@addSnapshotListener
            }

            for(cambios in datos!!.documentChanges){

                when(cambios.type){

                    DocumentChange.Type.ADDED -> Log.d("REALTIME", "agregado: ${cambios.document.data}")
                    DocumentChange.Type.MODIFIED -> Log.d("REALTIME", "modificado: ${cambios.document.data}")
                    DocumentChange.Type.REMOVED -> Log.d("REALTIME", "removido: ${cambios.document.data}")
                }
            }
        }
    }
}