package mx.tec.clase3

import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "nombre"
private const val ARG_PARAM2 = "edad"

/**
 * A simple [Fragment] subclass.
 * Use the [DoggoFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class DoggoFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var nombre: String? = null
    private var edad: Int = 0
    private var listener: Callback? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            nombre = it.getString(ARG_PARAM1)
            edad = it.getInt(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_doggo, container, false)

        // obtener referencia a widgets
        val nombreGui = view.findViewById<TextView>(R.id.doggo_nombre)
        val edadGui = view.findViewById<TextView>(R.id.doggo_edad)
        val boton = view.findViewById<Button>(R.id.doggo_boton)

        boton.setOnClickListener {

            Log.e("FRAGMENTO", "BOTONAZO!")

            // invocar método en listener
            listener?.ejecutar()
        }


        nombreGui.text = nombre
        edadGui.text = "$edad"

        return view
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)

        // agregar listener "automaticamente"
        listener = if(context is Callback){
            context
        } else {
            throw RuntimeException("ACTIVIDAD DEBE IMPLEMENTAR CALLBACK PARA USAR ESTE FRAGMENTO")
        }
    }

    public fun hacerLog(){

        Log.wtf("FRAGMENTO", "SÍ JALÓ!")
    }

    interface Callback {

        fun ejecutar()
    }

    companion object {

        @JvmStatic
        fun newInstance(nombre: String, edad: Int) =
            DoggoFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, nombre)
                    putInt(ARG_PARAM2, edad)
                }
            }
    }
}